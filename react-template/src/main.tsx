import * as React from 'react';
import { FC } from 'react';
import * as ReactDOM from 'react-dom';

import './main.css';

const App: FC = () => {
  return <p>Hello World</p>;
};

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
